package net.hexadigital.drawtest;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.InetAddress;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import net.hexadigital.drawtest.draw.DrawFrame;
import net.hexadigital.drawtest.network.DrawClient;
import net.hexadigital.drawtest.network.DrawServer;

@SuppressWarnings("serial")
public class MainGUI extends JFrame {
	public Executor executor = Executors.newCachedThreadPool();
	
	JButton hostButton;
	JButton joinButton;
	
	JTextField ipField;
	
	public DrawFrame drawFrame;
	
	public DrawServer server;
	public DrawClient client;
	
	public static void main(String[] args) {
		new MainGUI();
	}
	
	public MainGUI() {
		// Create the window
		super("Draw Demo");
		setLayout(new GridLayout(4, 1));
		
		// Center the window
		setLocationRelativeTo(null);
		
		// Set the program to exit when the window is closed
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Add content
		add(hostButton = new JButton("Host"));
		add(new JLabel("or", SwingConstants.CENTER));
		add(joinButton = new JButton("Join"));
		add(ipField = new JTextField("<ip address>"));
		ipField.setHorizontalAlignment(JTextField.CENTER);
		
		// Make ip field clear on click
		ipField.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (ipField.getText().equalsIgnoreCase("<ip address>")) {
					ipField.setText("");
				}
			}
		});
		
		// Handle enter button
		ipField.getRootPane().setDefaultButton(joinButton);
		
		// Add button listeners
		hostButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				startServer();
			}
		});
		
		joinButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				joinServer();
			}
		});
		
		
		// Display everything
		setSize(200,200);
		setVisible(true);
	}
	
	public void openDrawWindow(String host) {
		if (drawFrame == null) {
			// Make new window
			drawFrame = new DrawFrame(this, "Draw Server: " + host);
			
			// Make program close when drawing window is closed
			drawFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			
			// Handle window close
			drawFrame.addWindowListener(new WindowAdapter() {
				@Override
				public void windowClosing(WindowEvent e) {
					drawFrame = null;
					
					if (server != null) {
						server.stop();
						server = null;
					}
					if (client != null) {
						client.stop();
						client = null;
					}
				}
			});
		} else {
			JOptionPane.showMessageDialog(this, "There's already an open draw panel");
		}
	}

	public void startServer() {
		try {
			if (server == null) {
				server = new DrawServer(27018, this);
				
				openDrawWindow(InetAddress.getLocalHost().getHostAddress() + ":" + server.serverSocket.getLocalPort());
				
				// Start server network loop
				executor.execute(server);
			} else {
				JOptionPane.showMessageDialog(this, "There's already a running server");
			}
		} catch (IOException e) {
			JOptionPane.showMessageDialog(this, "Failed to start server");
			e.printStackTrace();
		}
	}
	
	public void joinServer() {
		try {
			if (client == null) {
				String[] hostAddress = ipField.getText().split(":");
				if (hostAddress.length > 1) {
					client = new DrawClient(hostAddress[0], Integer.parseInt(hostAddress[1]), this);
				} else {
					client = new DrawClient(hostAddress[0], 27018, this);
				}
				
				openDrawWindow(client.socket.getInetAddress().getHostAddress() + ":" + client.socket.getPort());
				
				// Start client network loop
				executor.execute(client);
			} else {
				JOptionPane.showMessageDialog(this, "There's already a running client");
			}
		} catch (IOException e) {
			JOptionPane.showMessageDialog(this, "Failed to start client");
			e.printStackTrace();
		}
	}
}
