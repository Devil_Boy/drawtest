package net.hexadigital.drawtest.blots;

import java.awt.Color;
import java.awt.Graphics;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * This is just a simple ball-point pen-tip
 * 
 * @author Devil Boy
 *
 */
public class Circle extends Blot {
	static final byte CIRCLE_ID = 0x01;
	
	Color color;
	int radius;
	
	public Circle(Location location, Color color, int radius) {
		super(CIRCLE_ID, location);
		
		this.color = color;
		this.radius = radius;
	}
	
	public Color getColor() {
		return color;
	}
	
	public int getRadius() {
		return radius;
	}

	@Override
	public void paintSelf(Graphics g) {
		g.setColor(color);
		g.fillOval(getRectX(), getRectY(), getRectWidth(), getRectHeight());
	}

	@Override
	public int getRectX() {
		return location.getX() - radius;
	}

	@Override
	public int getRectY() {
		return location.getY() - radius;
	}

	@Override
	public int getRectWidth() {
		return radius * 2;
	}

	@Override
	public int getRectHeight() {
		return radius * 2;
	}

	@Override
	void writeMetaToStream(DataOutputStream out) throws IOException {
		// First send the blot type (1 byte)
		out.writeByte(type);
		
		// Then send the color (4 bytes)
		out.writeInt(color.getRGB());
		
		// Then send the radius (4 bytes)
		out.writeInt(radius);
	}
}
