package net.hexadigital.drawtest.blots;

import java.awt.Color;
import java.awt.Graphics;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import net.hexadigital.drawtest.protocol.Streamable;

public abstract class Blot implements Streamable {
	
	byte type;
	Location location;
	
	public Blot(byte type, Location location) {
		this.type = type;
		this.location = location;
	}
	
	public Location getLocation() {
		return location;
	}

	public abstract void paintSelf(Graphics g);
	
	public abstract int getRectX();
	
	public abstract int getRectY();
	
	public abstract int getRectWidth();
	
	public abstract int getRectHeight();
	
	@Override
	public void writeToStream(DataOutputStream out) throws IOException {
		// Write the location
		location.writeToStream(out);
		
		// Write the type-specific data
		writeMetaToStream(out);
	}
	
	abstract void writeMetaToStream(DataOutputStream out) throws IOException;
	
	public static Blot readFromStream(DataInputStream in) throws IOException {
		Location location = Location.readFromStream(in);
		
		byte blotType = in.readByte();
		
		if (blotType == 0x01) {
			// It's a Circle
			int color = in.readInt();
			int radius = in.readInt();
			
			return new Circle(location, new Color(color), radius);
		} else {
			System.err.println("Got invalid blot type");
			return null;
		}
	}
}
