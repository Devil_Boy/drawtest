package net.hexadigital.drawtest.blots;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import net.hexadigital.drawtest.protocol.Streamable;

public class Location implements Streamable {
	private int xPos;
	private int yPos;
	
	public Location(int x, int y) {
		this.xPos = x;
		this.yPos = y;
	}
	
	public int getX() {
		return xPos;
	}
	
	public int getY() {
		return yPos;
	}

	@Override
	public void writeToStream(DataOutputStream out) throws IOException {
		// Just write the coordinates to the stream (8 bytes)
		out.writeInt(xPos);
		out.writeInt(yPos);
	}
	
	public static Location readFromStream(DataInputStream in) throws IOException {
		return new Location(in.readInt(), in.readInt());
	}
}
