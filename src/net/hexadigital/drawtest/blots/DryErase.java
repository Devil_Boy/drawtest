package net.hexadigital.drawtest.blots;

import java.awt.Graphics;
import java.io.DataOutputStream;
import java.io.IOException;

public class DryErase extends Blot {
	static final byte DRY_ERASE_ID = 0x02;
	
	public DryErase(Location location) {
		super(DRY_ERASE_ID, location);
	}

	@Override
	public void paintSelf(Graphics g) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getRectX() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getRectY() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getRectWidth() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getRectHeight() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	void writeMetaToStream(DataOutputStream out) throws IOException {
		// TODO Auto-generated method stub
		
	}

}
