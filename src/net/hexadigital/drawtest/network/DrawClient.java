package net.hexadigital.drawtest.network;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;

import net.hexadigital.drawtest.MainGUI;
import net.hexadigital.drawtest.protocol.Packet;
import net.hexadigital.drawtest.protocol.PacketHandler;

/**
 * This class represents a client connection
 * It is reused for both the server and client program
 * 
 * @author Devil Boy
 *
 */
public class DrawClient implements Runnable {
	
	public Socket socket;
	DataInputStream inStream;
	DataOutputStream outStream;
	
	public MainGUI mainGUI;
	
	PacketHandler packetHandler;
	
	// This is for when the program is a client
	public DrawClient(String host, int port, MainGUI mainGUI) throws UnknownHostException, IOException {
		this.socket = new Socket(host, port);
		this.mainGUI = mainGUI;
		
		this.packetHandler = new ClientPacketHandler(this);
		
		setupStreams();
	}
	
	// This is for when the program is a server
	public DrawClient(Socket socket, DrawServer server) throws IOException {
		this.socket = socket;
		this.mainGUI = server.mainGUI;
		
		this.packetHandler = new ServerPacketHandler(server, this);
		
		setupStreams();
	}
	
	void setupStreams() throws IOException {
		inStream = new DataInputStream(socket.getInputStream());
		outStream = new DataOutputStream(socket.getOutputStream());
	}
	
	public void send(Packet packet) {
		try {
			packet.writeToStream(outStream);
		} catch (IOException e) {
			System.err.println("Failed to write packet 0x" + Integer.toHexString(packet.id) + " to " + ((mainGUI.server == null) ? socket.getInetAddress().getHostAddress() : "server"));
			e.printStackTrace();
		}
	}
	
	public void stop() {
		// Close the connection
		try {
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		try {
			while (!socket.isClosed()) {
				// Get an incoming packet
				Packet packet = Packet.readFromStream(inStream);
				
				if (packet == null) {
					System.err.println("Error reading packet from stream");
				} else {
					// Handle the packet
					packetHandler.handlePacket(packet);
				}
			}
		} catch (Exception e) {
			// Close the socket (if it isn't already)
			stop();
			
			if (e instanceof SocketException || e instanceof EOFException) {
				if (mainGUI.server == null) {
					// The server disconnected
					System.out.println("The server disconnected");
				} else {
					// A client disconnected
					mainGUI.server.clients.remove(this);
					System.out.println("Client " + socket.getInetAddress().getHostAddress() + " disconnected");
				}
			} else {
				if (mainGUI.server == null) {
					// The server disconnected
					System.err.println("The server disconnected with exception");
				} else {
					// A client disconnected
					mainGUI.server.clients.remove(this);
					System.err.println("Client " + socket.getInetAddress().getHostAddress() + " disconnected with exception");
				}
				e.printStackTrace();
			}
		}
	}
}
