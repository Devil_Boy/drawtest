package net.hexadigital.drawtest.network;

import net.hexadigital.drawtest.protocol.Packet;
import net.hexadigital.drawtest.protocol.PacketHandler;
import net.hexadigital.drawtest.protocol.PacketSingleBlot;

public class ClientPacketHandler implements PacketHandler {
	DrawClient client;
	
	public ClientPacketHandler(DrawClient client) {
		this.client = client;
	}
	
	@Override
	public void handlePacket(Packet packet) {
		byte packetID = packet.id;
		
		if (packetID == 0x01) {
			PacketSingleBlot p = (PacketSingleBlot) packet;
			
			// Add the blot to the panel
			client.mainGUI.drawFrame.drawPanel.addSingleBlot(p.blot);
		} else {
			System.err.println("This client cannot handle packet 0x" + Integer.toHexString(packetID));
		}
	}
}
