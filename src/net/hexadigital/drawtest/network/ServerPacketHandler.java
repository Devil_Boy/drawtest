package net.hexadigital.drawtest.network;

import net.hexadigital.drawtest.protocol.Packet;
import net.hexadigital.drawtest.protocol.PacketHandler;
import net.hexadigital.drawtest.protocol.PacketSingleBlot;

public class ServerPacketHandler implements PacketHandler {
	DrawServer server;
	DrawClient client;
	
	public ServerPacketHandler(DrawServer server, DrawClient client) {
		this.server = server;
		this.client = client;
	}
	
	public void sendToPeers(Packet packet) {
		// Send the packet to the other clients
		for (DrawClient c : server.clients) {
			if (c != client) {
				c.send(packet);
			}
		}
	}
	
	@Override
	public void handlePacket(Packet packet) {
		byte packetID = packet.id;
		
		if (packetID == 0x01) {
			PacketSingleBlot p = (PacketSingleBlot) packet;
			
			// Add the blot to the panel
			server.mainGUI.drawFrame.drawPanel.addSingleBlot(p.blot);
			
			// Send the packet to the others
			sendToPeers(packet);
		} else {
			System.err.println("This server cannot handle packet 0x" + Integer.toHexString(packetID));
		}
	}
}
