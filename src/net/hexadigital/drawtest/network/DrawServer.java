package net.hexadigital.drawtest.network;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;

import net.hexadigital.drawtest.MainGUI;
import net.hexadigital.drawtest.protocol.Packet;

public class DrawServer implements Runnable {
	
	public ServerSocket serverSocket;
	boolean active;
	
	public List<DrawClient> clients = new ArrayList<DrawClient>();
	
	MainGUI mainGUI;
	
	public DrawServer(int port, MainGUI mainGUI) throws IOException {
		this.serverSocket = new ServerSocket(port);
		this.active = true;
		this.mainGUI = mainGUI;
	}
	
	public void broadcast(Packet packet) {
		for (DrawClient client : clients) {
			client.send(packet);
		}
	}
	
	public void stop() {
		active = false;
		
		// Stop client connections
		for (DrawClient client : clients) {
			client.stop();
		}
		
		// Unbind from the port
		try {
			serverSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		Socket socket;
		
		// Listen for clients
		while (active) {
			try {
				socket = serverSocket.accept();
				DrawClient client = new DrawClient(socket, this);
				clients.add(client);
				
				System.out.println("Client " + socket.getInetAddress().getHostAddress() + " connected");
				
				// Start client read loop
				mainGUI.executor.execute(client);
			} catch (Exception e) {
				if (e instanceof SocketException) {
					System.out.println("Socket closed");
				} else {
					System.err.println("Socket error");
					e.printStackTrace();
				}
			}
		}
	}
}
