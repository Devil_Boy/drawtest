package net.hexadigital.drawtest.protocol;

import java.io.DataOutputStream;
import java.io.IOException;

public interface Streamable {

	void writeToStream(DataOutputStream out) throws IOException;
}
