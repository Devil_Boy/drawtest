package net.hexadigital.drawtest.protocol;

import java.io.DataInputStream;
import java.io.IOException;

import net.hexadigital.drawtest.blots.Blot;


public abstract class Packet implements Streamable {
	public byte id;
	
	public Packet(byte id) {
		this.id = id;
	}
	
	public static Packet readFromStream(DataInputStream in) throws IOException {
		byte packetID = in.readByte();
		
		if (packetID == 0x01) {
			return new PacketSingleBlot(Blot.readFromStream(in));
		} else {
			System.err.println("Got invalid packet ID");
			return null;
		}
	}
}
