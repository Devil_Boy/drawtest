package net.hexadigital.drawtest.protocol;

import java.io.DataOutputStream;
import java.io.IOException;

import net.hexadigital.drawtest.blots.Blot;

/**
 * This packet merely sends a single blot
 * 
 * @author Devil Boy
 *
 */
public class PacketSingleBlot extends Packet {
	final static byte SINGLE_BLOT_ID = 0x01;
	
	public Blot blot;

	public PacketSingleBlot(Blot blot) {
		super(SINGLE_BLOT_ID);
		
		this.blot = blot;
	}

	@Override
	public void writeToStream(DataOutputStream out) throws IOException {
		// First write the packet type
		out.writeByte(id);
		
		// Send the blot data
		blot.writeToStream(out);
	}
}
