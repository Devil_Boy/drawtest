package net.hexadigital.drawtest.protocol;

public interface PacketHandler {

	void handlePacket(Packet packet);
}
