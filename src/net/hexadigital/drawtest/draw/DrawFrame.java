package net.hexadigital.drawtest.draw;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JColorChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import net.hexadigital.drawtest.MainGUI;

@SuppressWarnings("serial")
public class DrawFrame extends JFrame {
	MainGUI mainGUI;
	
	public DrawPanel drawPanel;
	
	public JPanel colorField;
	public JTextField sizeField;

	public DrawFrame(MainGUI mainGUI, String title) {
		super(title);
		
		this.mainGUI = mainGUI;
		
		init();
	}
	
	void init() {
		setLayout(new BorderLayout());
		
		// Add the drawing panel
		drawPanel = new DrawPanel(this);
		add(drawPanel, BorderLayout.CENTER);
		
		// Create top bar
		JPanel topBar = new JPanel();
		topBar.setLayout(new FlowLayout());
		add(topBar, BorderLayout.NORTH);
		
		// Add color to top bar
		final JPanel colorSection = new JPanel();
		colorSection.setLayout(new GridLayout(0,2));
		topBar.add(colorSection);
		colorSection.add(new JLabel("Color:"));
		
		// Set up color chooser and display
		topBar.add(colorField = new JPanel());
		colorField.setBackground(Color.RED);
		colorField.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				colorField.setBackground(JColorChooser.showDialog(colorSection, "Color chooser", colorField.getBackground()));
			}
		});
		
		// Add size to top bar
		JPanel sizeSection = new JPanel();
		sizeSection.setLayout(new GridLayout(0, 2));
		topBar.add(sizeSection);
		sizeSection.add(new JLabel("Size:"));
		sizeSection.add(sizeField = new JTextField("5", 5));
		sizeField.setHorizontalAlignment(JTextField.CENTER);
		
		// Make ip field clear on click
		sizeField.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				sizeField.setText("");
			}
		});
		
		// Center the window
		setLocationRelativeTo(null);
		
		// Display it
		pack();
		setVisible(true);
	}
}
