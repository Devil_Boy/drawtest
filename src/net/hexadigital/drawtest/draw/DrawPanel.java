package net.hexadigital.drawtest.draw;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Deque;
import java.util.concurrent.ConcurrentLinkedDeque;

import javax.swing.JPanel;

import net.hexadigital.drawtest.blots.Blot;
import net.hexadigital.drawtest.blots.Circle;
import net.hexadigital.drawtest.blots.Location;
import net.hexadigital.drawtest.protocol.PacketSingleBlot;

@SuppressWarnings("serial")
public class DrawPanel extends JPanel {
	
	DrawFrame drawFrame;
	
	Deque<Blot> blots;
	
	DrawProcessor drawProcessor;

	public DrawPanel(DrawFrame drawFrame) {
		this.drawFrame = drawFrame;
		this.blots = new ConcurrentLinkedDeque<Blot>();
		this.drawProcessor = new DrawProcessor(this);
		
		// Start processing thread
		drawFrame.mainGUI.executor.execute(drawProcessor);
		
		// Set listeners
		addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				drawProcessor.addMove(new PenMove(new Location(e.getX(), e.getY()), true));
			}
		});
		
		addMouseMotionListener(new MouseAdapter() {
			@Override
			public void mouseDragged(MouseEvent e) {
				drawProcessor.addMove(new PenMove(new Location(e.getX(), e.getY()), false));
			}
		});
	}
	
	@Override
	public Dimension getPreferredSize() {
		return new Dimension(800, 600);
	}
	
	
	public void mark(Location location) {
		// Check given size
		int size = 5;
		try {
			size = Integer.parseInt(drawFrame.sizeField.getText());
		} catch (NumberFormatException e) {
			drawFrame.sizeField.setText("5");
		}
		
		Blot b = new Circle(location, drawFrame.colorField.getBackground(), size);
		addSingleBlot(b);
		
		// If server, send blot to all clients
		if (drawFrame.mainGUI.server != null) {
			drawFrame.mainGUI.server.broadcast(new PacketSingleBlot(b));
		}
		
		// If client, send blot to server
		if (drawFrame.mainGUI.client != null) {
			drawFrame.mainGUI.client.send(new PacketSingleBlot(b));
		}
		
    }
	
	public void mark (int x, int y) {
		mark(new Location(x, y));
	}
	
	public void addSingleBlot(Blot blot) {
		blots.add(blot);
		repaint(blot.getRectX(), blot.getRectY(), blot.getRectWidth(), blot.getRectHeight());
	}
    
	@Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        for (Blot b : blots) {
        	b.paintSelf(g);
        }
    }
	
	public void drawLine(Location to, Location from) {
		//Draw vertical lines
		//(slope will be undefined)
		if (to.getX() == from.getX()) {
			if (from.getY() < to.getY()) {
				for (int i = from.getY(); i <= to.getY(); i++) {
					mark(from.getX(), i);
				}
			}
			else {
				for (int i = from.getY(); i >= to.getY(); i--) {
					mark(from.getX(), i);
				}
			}
		}
		else {
			double slope = 0;
			
			//Calculate slope since we know it isn't undefined
			slope = (double) (to.getY()-from.getY())/(double)(to.getX()-from.getX());
			
			//Iterate through x values if slope <= 1
			if (Math.abs(slope) <= 1) {
				//Draw to the right
				if (to.getX()-from.getX() > 0) {
					for (int i = from.getX(); i <= to.getX(); i++) {
						mark(i, (int) (from.getY() + slope*((double) i-from.getX())));
					}
				}
				//Draw to the left
				else if (to.getX()-from.getX() < 0) {
					for (int i = from.getX(); i >= to.getX(); i--) {
						mark(i, (int) (from.getY() + slope*((double) i-from.getX())));
					}
				}
			}//Otherwise iterate through y values 
			else if (Math.abs(slope) > 1 ){
				if (to.getY()-from.getY() > 0) {
					for (int i = from.getY(); i <= to.getY(); i++) {
						mark((int) (from.getX() + (1/slope)*((double) i-from.getY())), i);
					}
				}
				else if (to.getY()-from.getY() < 0) {
					for (int i = from.getY(); i >= to.getY(); i--) {
						mark((int) (from.getX() + (1/slope)*((double) i-from.getY())), i);
					}
				}
			}
		}
	}
	
	
}
