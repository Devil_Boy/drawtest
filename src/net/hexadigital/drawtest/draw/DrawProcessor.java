package net.hexadigital.drawtest.draw;

import java.awt.Color;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class DrawProcessor implements Runnable {
	private volatile boolean isRunning = true;
	
	DrawPanel drawPanel;
	
	private Queue<PenMove> toProcess;
	private PenMove lastMove;
	
	public DrawProcessor(DrawPanel drawPanel) {
		this.drawPanel = drawPanel;
		this.toProcess = new ConcurrentLinkedQueue<PenMove>();
	}
	
	public synchronized void addMove(PenMove move) {
		toProcess.offer(move);
		notifyAll();
	}

	@Override
	public void run() {
		processMove();
	}
	
	synchronized void processMove() {
		PenMove move;
		
		while (isRunning) {
			// Wait until somebody adds to the queue
			try {
				wait();
			} catch (InterruptedException e) {}
			
			while ((move = toProcess.poll()) != null) {
				if (move.firstPress) {
					drawPanel.mark(move.to);
				}
				else {
					drawPanel.drawLine(move.to, lastMove.to);
				}

				lastMove = move;
			}
		}
	}
	
	public void kill() {
		isRunning = false;
	}

}
