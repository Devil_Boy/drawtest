package net.hexadigital.drawtest.draw;

import net.hexadigital.drawtest.blots.Location;

public class PenMove {
	Location to;
	boolean firstPress;
	
	public PenMove(Location to, boolean firstPress) {
		this.to = to;
		this.firstPress = firstPress;
	}
}
